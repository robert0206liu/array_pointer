#include <stdio.h>

void foo(int [][3]);

int main(void) 
{
    int a[3][3] = {{1,2,3}, {4,5,6}, {7,8,9}};
    int i,j;
    printf("a       = %p\n", a);
    printf("a[0][0] = %p\n", &a[0][0]);
    printf("a[0][1] = %p\n", &a[0][1]);
    printf("a[1][1] = %p\n", &a[1][1]);
    printf("a[2][2] = %p\n", &a[2][2]);
    printf("\n");
    
    foo(a);
    printf("\n");

    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("%d,", a[i][j]);
        }
        printf("\n");
    }
    return 0;
}

void foo(int b[][3])
{
    printf("b       = %p\n", b);
    ++b;
    printf("++b     = %p\n", b);
    printf("b[1][1] = %p\n", &b[1][1]);
    b[1][1]=100;
}
